package edu.ucsd.cs110s.temperature; 
/**
 * @author yug007@ucsd.edu
 *
 */
public class Celsius extends Temperature{ 
	public Celsius(float t){ 
		super(t);  
	} 
	
	public String toString(){ 
		// TODO: Complete this method 
		return Float.toString(getValue());
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit((float) (this.getValue()*9.0/5.0+32.0));
	} 
} 
