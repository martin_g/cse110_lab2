package edu.ucsd.cs110s.temperature; 
/**
 * @author mez023@ucsd.edu
 *
 */
public class Fahrenheit extends Temperature { 
	public Fahrenheit(float t){ 
		super(t); 
	} 
	public String toString(){ 
		return Float.toString(this.getValue());
	}
	
	@Override
	public Temperature toCelsius() {
		return new Celsius((float) ((float) (this.getValue()-32.0) * 5.0 /9.0));
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	} 
} 
